'use strict';

const express = require('express');
const multer = require('multer');
const cors = require('cors');
const fs = require('fs');
const https = require('https');
const MongoClient = require('mongodb').MongoClient;
const bodyParser = require('body-parser');
const NodeGoogleDrive = require('node-google-drive');

// Constants
const PORT = 6701;
const HOST = 'localhost';

// App
const app = express();
app.use(cors());
app.use(bodyParser.json());

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads');
  },
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}-${file.originalname}`);
  },
});

const upload = multer({ storage });
app.post('/upload', upload.single('file'), (req, res, next) => {
  const file = req.file;
  if (!file) {
    return res.status(400).send({ error: `Please upload a file` });
  }
  console.dir(file);
  res.send(file);
});

app.post('/uploadToDrive', upload.single('file'), async (req, res, next) => {
  console.log('uploadToDrive called');
  const file = req.file;
  if (!file) {
    return res.status(400).send({ error: `Please upload a file` });
  }
  console.dir(file);

  await writeFileToGoogleDrive(file);
  fs.unlinkSync(file.path);
  res.send(file);
});

app.get('/', (req, res) => {
  res.send('Hello from Node Service 1');
});

MongoClient.connect('mongodb://localhost:27017', (err, client) => {
  if (err) return console.log(err);

  app.post('/upload3', upload.single('file'), async (req, res) => {
    const file = req.file;
    if (!file) {
      return res.status(400).send({ error: `Please upload a file` });
    }

    console.dir(file);
    const content = fs.readFileSync(file.path);
    const encode_content = content.toString('base64');

    // Define a JSONobject for the image attributes for saving to database
    const object = {
      filename: file.originalname,
      contentType: file.mimetype,
      data: new Buffer(encode_content, 'base64'),
    };

    try {
      const db = client.db('ijooz');
      const result = await db.collection('images').insertOne(object);
      console.log(`saved to database: ${result.insertedId.toString()}`);

      fs.unlinkSync(file.path);

      res.send(file);
    } catch (error) {
      console.log(error);
      return res.status(500).send({ error: `Failed to save file` });
    }
  });

  app.get('/download/:filename', async (req, res) => {
    const { filename } = req.params;
    if (!filename) {
      return res.status(400).send({ error: `Filename not specified` });
    }

    try {
      const db = client.db('ijooz');
      const result = await db
        .collection('images')
        .findOne({ filename: filename });
      res.contentType(result.contentType);
      res.send(result.data.buffer);
    } catch (error) {
      console.log(error);
      return res.status(500).send({ error: `Failed to retrieve file` });
    }
  });

  https
    .createServer(
      {
        key: fs.readFileSync('server_dev.key'),
        cert: fs.readFileSync('server_dev.crt'),
      },
      app
    )
    .listen(PORT, HOST);
  console.log(`Running on https://${HOST}:${PORT}`);
});

app.post('/locations', (req, res) => {
  console.log(`Headers: ${JSON.stringify(req.headers)}`);
  console.log(`Locations: ${JSON.stringify(req.body)}`);
  res.sendStatus(200);
});

const ROOT_FOLDER = '1lOjYwb7kBD6W7EBaViBbwTyZ9RT5b15W';
async function testGoogleDrive() {
  const client_credential = require('./ijooz-ops-google-drive.json');
  const googleDriveInstance = new NodeGoogleDrive();

  const gdrive = await googleDriveInstance.useServiceAccountAuth(
    client_credential
  );

  // const folderResponse = await googleDriveInstance.listFolders(
  //   ROOT_FOLDER,
  //   null,
  //   false
  // );
  // console.dir(folderResponse);

  const newFolder = { name: 'xxxxxx' + Date.now() };
  const createFolderResponse = await googleDriveInstance.createFolder(
    ROOT_FOLDER, //'1s_oGwBeaNcjKincIo_grRfrJfcUWshRw',
    newFolder.name
  );
  console.dir(createFolderResponse);

  const listFilesResponse = await googleDriveInstance
    .listFiles
    // '1jmYh188Rc4VKqDR0n10bDwRJ_KaHbROc',
    // null,
    // false
    ();
  console.dir(listFilesResponse);

  // const writeFileResponse = await googleDriveInstance.writeFile(
  //   './reference.txt',
  //   '1jmYh188Rc4VKqDR0n10bDwRJ_KaHbROc',
  //   'hello2.txt',
  //   'text/plain'
  // );
  // console.dir(writeFileResponse);

  // const listFilesResponse2 = await googleDriveInstance.listFiles(
  //   '1jmYh188Rc4VKqDR0n10bDwRJ_KaHbROc',
  //   null,
  //   false
  // );
  // console.dir(listFilesResponse2);

  // for (let file of listFilesResponse2.files) {
  //   console.log(file.name);
  //   console.dir(file.parents);
  // }
}

// const ROOT_FOLDER = '1lOjYwb7kBD6W7EBaViBbwTyZ9RT5b15W';
const TASK_FOLDER = '1APZpcbkrKrDwZJx9smFq49XaNjvvcl7E'; // 'tasks' folder
async function writeFileToGoogleDrive(file) {
  const client_credential = require('./ijooz-ops-google-drive.json');
  const googleDriveInstance = new NodeGoogleDrive({
    ROOT_FOLDER,
  });

  const gdrive = await googleDriveInstance.useServiceAccountAuth(
    client_credential
  );

  const writeFileResponse = await googleDriveInstance.writeFile(
    file.path,
    TASK_FOLDER,
    file.originalname,
    file.mimetype
  );
  console.dir(writeFileResponse);

  const listFilesResponse = await googleDriveInstance.listFiles(
    TASK_FOLDER,
    null,
    false
  );
  console.dir(listFilesResponse);
}

// (async () => {
//   try {
//     await testGoogleDrive();
//   } catch (error) {
//     console.log(error);
//   }
// })();

const consulHelper = require('ijooz-consul');
consulHelper.registerService({ name: 'NodeService1', port: PORT });
